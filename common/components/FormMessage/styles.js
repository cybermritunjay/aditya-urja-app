
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    error: {
      color: 'red',
      marginBottom: 20,
    },
    errorContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20,
    },
  })

  export default styles;
